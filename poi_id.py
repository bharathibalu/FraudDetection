#!/usr/bin/python

import sys
import pickle
sys.path.append(r'C:\Users\bhara\repos\ud120-projects\tools')

from feature_format import featureFormat, targetFeatureSplit
from tester import dump_classifier_and_data,test_classifier

class dataset:
    def __init__(self):
        ### Load the dictionary containing the dataset
        with open(r'C:\Users\bhara\repos\ud120-projects\final_project\final_project_dataset.pkl', "r") as data_file:
            ### Store to my_dataset for easy export below.
            self.my_dataset = pickle.load(data_file)

        #self.my_dataset = self.data_dict
        self.employees =  self.my_dataset.keys()

    def outlier_removal(self):
        ### Task 1: Remove outliers

         #Features and the number of NaN
        features = self.my_dataset[self.employees[0]].keys()
        print "\nCount of number of NaN in each feature:"
        for feature in features:
            count = 0
            for employee in self.employees:
                if self.my_dataset[employee][feature] == 'NaN':
                    count = count + 1
            print feature, ":", count
      
        max_salary = 0
        max_salary_employee =""
        for employee in self.employees:
            if self.my_dataset[employee]["salary"] > max_salary and self.my_dataset[employee]["salary"] != "NaN":
                max_salary = self.my_dataset[employee]["salary"]
                max_salary_employee = employee

        print "\nEmployee with highest salary: ",max_salary_employee       
        del self.my_dataset[max_salary_employee]
        self.employees.remove(max_salary_employee)

    def feature_creation(self):
        ### Task 2: Create new feature(s)
        #Fraction of emails sent to and from POI. Convert the fraction to percentage
        for employee in self.employees:
            if  self.my_dataset[employee]["from_poi_to_this_person"] == "NaN" or self.my_dataset[employee]["to_messages"] == "NaN":
                email_percentage_from_poi = 0
            else:
                email_percentage_from_poi = self.my_dataset[employee]["from_poi_to_this_person"]*100/self.my_dataset[employee]["to_messages"]

            if  self.my_dataset[employee]["from_this_person_to_poi"] == "NaN" or self.my_dataset[employee]["from_messages"] == "NaN":
                email_percentage_to_poi = 0
            else:   
                email_percentage_to_poi = self.my_dataset[employee]["from_this_person_to_poi"]*100/self.my_dataset[employee]["from_messages"]
            self.my_dataset[employee].update({"email_percentage_to_poi":email_percentage_to_poi,"email_percentage_from_poi":email_percentage_from_poi})

    def dimentionality_reduction(self):
        ####Task 3: PCA
        #PCA between bonus and long_term_incentive
        features_list = ['poi','bonus','long_term_incentive'] 
        data = featureFormat(self.my_dataset, features_list,remove_all_zeroes=False)
        labels, features = targetFeatureSplit(data)

        from sklearn.decomposition import PCA
        pca = PCA(n_components=1)
        incentive = pca.fit_transform(features)

        #PCA between email_percentage_from_poi and email_percentage_to_poi
        features_list = ['poi','email_percentage_from_poi','email_percentage_to_poi'] # You will need to use more features
        data = featureFormat(self.my_dataset, features_list,remove_all_zeroes=False)
        labels, features = targetFeatureSplit(data)

        pca = PCA(n_components=1)
        email_percentage_with_poi = pca.fit_transform(features)

        for i, employee in enumerate(self.employees):
            self.my_dataset[employee].update({"incentive":incentive[i][0],"email_percentage_with_poi":email_percentage_with_poi[i][0]})

    def feature_scaling(self):
        ####Task 4:Fearure Scaling
        #features_list = ['poi','exercised_stock_options','salary','bonus','email_percentage_to_poi','long_term_incentive','restricted_stock','shared_receipt_with_poi','email_percentage_from_poi'] # You will need to use more features
        #features_list = ['poi','exercised_stock_options','salary','bonus','long_term_incentive','email_percentage_to_poi','email_percentage_from_poi','incentive','email_percentage_with_poi','restricted_stock','shared_receipt_with_poi'] # You will need to use more features
        features_list = ['poi','exercised_stock_options','salary','incentive','email_percentage_with_poi','restricted_stock','shared_receipt_with_poi'] # You will need to use more features
        data = featureFormat(self.my_dataset, features_list,remove_all_zeroes=False)
        labels, features = targetFeatureSplit(data)

        from sklearn.preprocessing import MinMaxScaler
        scaler = MinMaxScaler()
        scaled_features = scaler.fit_transform(features,labels)

        for i,employee in enumerate(self.employees):
            self.my_dataset[employee].update({"exercised_stock_options":scaled_features[i][0],"salary":scaled_features[i][1],"incentive":scaled_features[i][2],"email_percentage_with_poi":scaled_features[i][3],"restricted_stock":scaled_features[i][4],"shared_receipt_with_poi":scaled_features[i][5]})

    def feature_selection(self):
        ### Task 5: Feature selection
        features_list = ['poi','exercised_stock_options','salary','incentive','email_percentage_with_poi','restricted_stock','shared_receipt_with_poi'] # You will need to use more features
        data = featureFormat(self.my_dataset, features_list)
        labels, features = targetFeatureSplit(data)

        # Select what features you'll use.
        ### features_list is a list of strings, each of which is a feature name.
        ### The first feature must be "poi".
        #5 features give the maximum accuracy. Accuracy drops with more features
        from sklearn.feature_selection import SelectKBest
        selector = SelectKBest(k=5)
        best_features = selector.fit_transform(features,labels)
        selectedIndices = selector.get_support(indices=True)

        features_list.remove('poi')
        print "\nFeatures selected by me:",features_list
        print "Scores of features:",selector.scores_
        selected_features_list = [features_list[i] for i in selectedIndices]
        print "Algo selected features:",selected_features_list
        selected_features_list.insert( 0, 'poi')
        return selected_features_list

class classifier:
    def __init__(self):
        self.PERF_FORMAT_STRING = "\
\tAccuracy: {:>0.{display_precision}f}\tPrecision: {:>0.{display_precision}f}\t\
Recall: {:>0.{display_precision}f}\tF1: {:>0.{display_precision}f}"

    def train_test_split_validation(self, clf, my_dataset, features_list):
        ### Task 7: Fit and Validate classifier
        ### Tune your classifier to achieve better than .3 precision and recall 
        ### using our testing script. Check the tester.py script in the final project
        ### folder for details on the evaluation method, especially the test_classifier
        ### function. Because of the small size of the dataset, the script uses
        ### stratified shuffle split cross validation. For more info: 
        ### http://scikit-learn.org/stable/modules/generated/sklearn.cross_validation.StratifiedShuffleSplit.html
        data = featureFormat(my_dataset, features_list)
        labels, features = targetFeatureSplit(data)

        # Example starting point. Try investigating other evaluation techniques!
        from sklearn.cross_validation import train_test_split
        features_train, features_test, labels_train, labels_test = \
            train_test_split(features, labels, test_size=0.3, random_state=42)

        print clf
        clf.fit(features_train,labels_train)
        #print "Selected params: ",clf.best_params_
        predictions = clf.predict(features_test)
        from sklearn.metrics import accuracy_score,precision_score,recall_score,f1_score
        accuracy = accuracy_score(labels_test,predictions)
        precision = precision_score(labels_test, predictions)
        recall = recall_score(labels_test,predictions)
        f1 = f1_score(labels_test,predictions)
        #feature_imp = clf.feature_importances_
        #print "Feature importance: ",feature_imp
        print self.PERF_FORMAT_STRING.format(accuracy, precision, recall, f1, display_precision = 5)

    def classifier_creation(self,my_dataset,features_list):
        ### Task 6: Try a varity of classifiers
        ### Please name your classifier clf for easy export below.
        ### Note that if you want to do PCA or other multi-stage operations,
        ### you'll need to use Pipelines. For more info:
        ### http://scikit-learn.org/stable/modules/pipeline.html

        #features_list = ['poi','exercised_stock_options','salary','incentive','email_percentage_with_poi','restricted_stock']
        #features_list = ['poi','exercised_stock_options','salary','bonus','email_percentage_to_poi','long_term_incentive','restricted_stock']

        # Provided to give you a starting point. Try a variety of classifiers.
        #from sklearn.model_selection import GridSearchCV
        from sklearn import grid_search
        from sklearn.naive_bayes import GaussianNB
        from sklearn.tree import DecisionTreeClassifier 
        from sklearn.neighbors import KNeighborsClassifier
        from sklearn.ensemble import AdaBoostClassifier
        from sklearn.ensemble import RandomForestClassifier 
        classifier_list = [
                        DecisionTreeClassifier(min_samples_split=2,max_depth=10,criterion="entropy",min_samples_leaf=3,max_leaf_nodes=40,random_state=42),
                        KNeighborsClassifier(),
                        #AdaBoostClassifier(), # removed in the final submission since it takes a lot of time to run
                        #RandomForestClassifier(),# removed in the final submission since it takes a lot of time to run
                        GaussianNB()]

        for clf in classifier_list:
            #Cross Validation
            test_classifier(clf, my_dataset, features_list)
            self.train_test_split_validation (clf, my_dataset, features_list)
        print "\nGaussianNB() gives the best output compared to other types of classifiers. So GaussianNB is selected"

        ### Task 8: Dump your classifier, dataset, and features_list so anyone can
        ### check your results. You do not need to change anything below, but make sure
        ### that the version of poi_id.py that you submit can be run on its own and
        ### generates the necessary .pkl files for validating your results.

        dump_classifier_and_data(clf, my_dataset, features_list)

def main():
    data = dataset()
    data.outlier_removal()
    data.feature_creation()
    data.dimentionality_reduction()
    data.feature_scaling()
    features_list = data.feature_selection()

    cla = classifier()
    cla.classifier_creation(data.my_dataset,features_list)


if __name__ == '__main__':
    main()